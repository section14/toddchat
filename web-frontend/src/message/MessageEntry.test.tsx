import { fireEvent, render, screen } from "@testing-library/react";
import MessageEntry from "./MessageEntry";

test("ensure entered message triggers a callback", () => {
  const cb = jest.fn();
  render(<MessageEntry callback={cb} />);

  let msgInput = screen.getByRole("textbox") as HTMLInputElement;
  fireEvent.change(msgInput, { target: { value: "test message" } });
  expect(msgInput.value).toBe("test message");

  fireEvent.keyDown(msgInput, { key: "Enter", code: "Enter" });
  expect(cb).toBeCalledTimes(1);
});
