export interface Message {
  content: string;
  time: string;
  me: boolean;
}

const MessageLine = (props: { msg: string; me: boolean }) => {
  return (
    <div
      className={`row ${
        props.me ? "justify-content-end" : "justify-content-start"
      }`}
    >
      <div
        className={`col-md-6 mt-3 p-3 msg-bubble ${
          props.me ? "text-end" : "text-start"
        } ${props.me ? "me-bubble-color" : "them-bubble-color"}`}
      >
        {props.msg}
      </div>
    </div>
  );
};

const MessageList = (props: { messages: Message[] }) => {
  return (
    <div>
      <ul>
        {props.messages.map((msg) => (
          <li key={Math.random().toString(36).substr(2, 9)}>
            <MessageLine msg={msg.content} me={msg.me} />
          </li>
        ))}
      </ul>
    </div>
  );
};

export default MessageList;
