import Picker from "emoji-picker-react";
import { useState, useRef, useEffect } from "react";

const MessageEntry = (props: { callback: (msg: string) => void }) => {
  const [msg, setMessage] = useState("");
  const msgInput = useRef<HTMLInputElement>(null);

  //todo: find a way to type this? It wouldn't
  //let me use KeyboardEvent because it's actually
  //KeyboardEvent<HTMLInputElement>. But, I couldn't
  //use that either because it's not generic. What
  //is the point of typescript again?

  //add emoji to message
  const addEmoji = (_event: any, object: any) => {
    const tmpMsg = msg + object.emoji;
    setMessage(tmpMsg);
  };

  //send message via callback supplied by parent
  const sendMessage = (e: any) => {
    if (e.key === "Enter") {
      props.callback(msg);
      setMessage("");
      msgInput.current?.focus();
    }
  };

  //onload event
  useEffect(() => {
    msgInput.current?.focus();
  }, []);

  return (
    <div className="row">
      <div className="col mt-3 mb-4">
        <div className="mb-3">
          {/*<Picker
            onEmojiClick={(e, o) => {
              addEmoji(e, o);
            }}
          />*/}
        </div>

        <input
          type="text"
          value={msg}
          ref={msgInput}
          onChange={(event) => setMessage(event.target.value)}
          onKeyDown={(event) => sendMessage(event)}
          className="form-control"
        />
      </div>
    </div>
  );
};

export default MessageEntry;
