import { render, screen } from "@testing-library/react";
import { Message } from "./MessageList";
import MessageList from "./MessageList";

const messages: Message[] = [
  { content: "test message 01", time: "", me: false },
  { content: "test message 02", time: "", me: true },
  { content: "test message 03", time: "", me: false },
];

test("render messages in MessageList", () => {
  render(<MessageList messages={messages} />);
  expect(screen.getByText("test message 01")).toBeInTheDocument();
  expect(screen.getByText("test message 02")).toBeInTheDocument();
  expect(screen.getByText("test message 03")).toBeInTheDocument();
});
