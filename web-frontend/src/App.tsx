//import './App.css';
import { useEffect, useState } from "react";
import ChatDashboard from "./ChatDashboard";
import LoginWindow from "./user/LoginWindow";
import { api, verifyToken } from "./request/http";

function App() {
  const [loggedIn, userLogin] = useState(false);

  //startup
  useEffect(() => {
    //check logged in state

    //localStorage.setItem("token", "")
    const token = localStorage.getItem("token");

    //return on no token available
    if (token === null || token === "") {
      userLogin(false);
      return;
    }

    checkToken(token);
  }, []);

  //function to check jwt token and verify login
  const checkToken = async (token: string) => {
    const res = await verifyToken(`${api()}/`, token!).catch((e: any) => {
      //idk
      console.log("bad chat token: ", e.message);
    });

    if (res) {
      userLogin(true);
    }
  };

  //callback for successful logins
  const loginCallback = (ok: boolean) => {
    if (ok) {
      userLogin(true);
      return;
    }

    userLogin(false);
  };

  return (
    <>
      <div className="container-fluid vh-100">
        <div className="row justify-content-center vh-100">
          <div className="col">
            {loggedIn ? null : <LoginWindow callback={loginCallback} />}
            {loggedIn ? <ChatDashboard /> : null}
          </div>
        </div>
      </div>
    </>
  );
}

export default App;
