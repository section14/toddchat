import { useEffect, useState } from "react";
import { getAuthData, api } from "../request/http";

interface User {
  id: number;
  name: string;
  current: boolean;
}

const UserItem = (props: { username: string; current: boolean }) => {
  return (
    <>
      <div
        className={`${props.current ? "bg-light-grey-3" : "bg-light"} h5 p-2`}
      >
        {props.username}
      </div>
    </>
  );
};

const UserList = (props: { callback: (msg: number) => void }) => {
  //startup call
  useEffect(() => {
    getUsers();
  }, []);

  const [users, setUser] = useState<User[]>([]);

  //get all users that aren't the current user
  const getUsers = async () => {
    const token = localStorage.getItem("token");

    const res = await getAuthData(`${api()}/users/get/not`, token!).catch(
      (e: any) => {
        console.log("couldn't get users: ", e.message);
      }
    );

    if (res) {
      addUsers(res);
    }
  };

  //add returned users to list for selection
  const addUsers = (res: any) => {
    let chatUsers = res.map(
      (usr: any) =>
        ({
          id: usr.id,
          name: usr.name,
        } as User)
    );

    setUser(chatUsers);
  };

  //pass selected user up with callback function
  const selectUser = (key: number) => {
    props.callback(key);

    users.forEach((user) => {
      if (user.id === key) {
        user.current = true;
      } else {
        user.current = false;
      }
    });
  };

  //logout
  const logout = () => {
    localStorage.setItem("token", "");
    window.location.reload();
  };

  return (
    <div className="p-3">
      <h3>
        <span className="mr-3">Chats</span>
        &nbsp;
        <i className="ml-3 material-icons text-success">chat_bubble</i>
      </h3>
      <div className="link-hover" onClick={() => logout()}>
        Logoout
      </div>
      <hr />
      <ul>
        {users.map((user, i) => (
          <li
            className="link-hover"
            key={i}
            onClick={() => selectUser(user.id)}
          >
            <UserItem username={user.name} current={user.current} />
          </li>
        ))}
      </ul>
    </div>
  );
};

export default UserList;
