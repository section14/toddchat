import { fireEvent, render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { rest } from "msw";
import { setupServer } from "msw/node";
import LoginWindow from "./LoginWindow";
import { api } from "../request/http";

const server = setupServer(
  rest.post(`${api()}/login`, (_req, res, ctx) => {
    return res(
      ctx.delay(200),
      ctx.status(202, "mocked request"),
      ctx.json({ token: "passed_token" })
    );
  })
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

test("verify login form fields", () => {
  render(<LoginWindow callback={() => {}} />);

  const username = screen.getByLabelText("username") as HTMLInputElement;
  fireEvent.change(username, { target: { value: "testname" } });
  expect(username.value).toBe("testname");

  const password = screen.getByLabelText("password") as HTMLInputElement;
  fireEvent.change(password, { target: { value: "testpass" } });
  expect(password.value).toBe("testpass");
});

test("verify form submit", async () => {
  render(<LoginWindow callback={() => {}} />);

  userEvent.click(screen.getByRole("button", { name: /Login/i }));
  expect(await screen.findByText("logged in")).toBeInTheDocument();
});
