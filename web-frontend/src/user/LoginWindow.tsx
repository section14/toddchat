import { useState, useEffect } from "react";
import { api, postData } from "../request/http";

const LoginWindow = (props: { callback: (ok: boolean) => void }) => {
  interface Login {
    username: string;
    password: string;
  }

  const [loginData, setLogin] = useState<Login>({ username: "", password: "" });
  const [loginStatus, setStatus] = useState("");

  //start up
  useEffect(() => {
    console.log("app started");
  }, []);

  //capture keyboard enter keydown event
  useEffect(() => {
    const captureEnter = (e: KeyboardEvent) => {
      if (e.key === "Enter") {
        loginUser();
      }
    };

    window.addEventListener("keydown", captureEnter);

    return () => {
      window.removeEventListener("keydown", captureEnter);
    };
  });

  //handle form change
  const handleInputChange = (e: any) => {
    const { name, value } = e.currentTarget;
    setLogin({ ...loginData, [name]: value });
  };

  //attempt to log user in
  const loginUser = async () => {
    let res = await postData(`${api()}/login`, loginData).catch((e: any) => {
      console.log("login error: ", e.message);
      return;
    });

    //check for undefined response
    if (res === undefined) {
      setStatus("login failed");
      props.callback(false);
      return;
    }

    //set status - mainly for testing
    setStatus("logged in");

    //set user token
    //todo: save this in a cookie
    localStorage.setItem("token", res.token);

    //signal to callback we logged in
    props.callback(true);
  };

  return (
    <div>
      <div>Login</div>

      <div className="row">
        <div className="col-md-6">
          <div className="mt-5">
            <label htmlFor="user-input">username</label>
            <input
              id="user-input"
							name="username"
              type="text"
              onChange={(e) => handleInputChange(e)}
              className="form-control"
            />
          </div>

          <div className="mt-5">
            <label htmlFor="password-input">password</label>
            <input
              id="password-input"
							name="password"
              type="password"
              onChange={(e) => handleInputChange(e)}
              className="form-control"
            />
          </div>
        </div>
      </div>

      <button onClick={() => loginUser()} className="btn mt-3 btn-primary">
        Login
      </button>

      <div className="row">
        <div className="col">{loginStatus}</div>
      </div>
    </div>
  );
};

export default LoginWindow;
