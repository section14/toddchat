import { render, screen } from "@testing-library/react";
import { rest } from "msw";
import { setupServer } from "msw/node";
import UserList from "./UserList";
import { api } from "../request/http";

const server = setupServer(
  rest.get(`${api()}/users/get/not`, (_req, res, ctx) => {
    return res(
      ctx.delay(200),
      ctx.status(202, "mocked request"),
      ctx.json([
        { id: 1, name: "test1" },
        { id: 2, name: "test2" },
        { id: 3, name: "test3" },
      ])
    );
  })
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

test("get list of users", async () => {
  render(<UserList callback={() => {}} />);

  expect(await screen.findByText("test1")).toBeInTheDocument();
  expect(await screen.findByText("test2")).toBeInTheDocument();
  expect(await screen.findByText("test3")).toBeInTheDocument();
});
