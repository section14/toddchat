import { render, screen } from "@testing-library/react";
import { rest } from "msw";
import WS from "jest-websocket-mock";
import { setupServer } from "msw/node";
import ChatWindow from "./ChatWindow";
import { api, socket } from "./request/http";

const server = setupServer(
  rest.get(`${api()}/messages/user/1`, (_req, res, ctx) => {
    return res(
      ctx.delay(200),
      ctx.status(202, "mocked request"),
      ctx.json([
        {
          id: 0,
          value: "test message 1",
          user_id: 1,
          room_id: 0,
          timestamp: "",
        },
        {
          id: 0,
          value: "test message 2",
          user_id: 2,
          room_id: 0,
          timestamp: "",
        },
        {
          id: 0,
          value: "test message 3",
          user_id: 1,
          room_id: 0,
          timestamp: "",
        },
      ])
    );
  }),

  rest.get(`${api()}/chat/token/create`, (_req, res, ctx) => {
    return res(
      ctx.delay(200),
      ctx.status(202, "mocked request"),
      ctx.json({ user_id: 1, chat_token: "testToken" })
    );
  })
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

test("fetch previous chat messages", async () => {
  render(<ChatWindow user={1} />);

  const ws_server = new WS(`${socket()}/chat/testToken/user/1`);
  await ws_server.connected;

  expect(await screen.findByText("test message 1")).toBeInTheDocument();
  expect(await screen.findByText("test message 2")).toBeInTheDocument();
  expect(await screen.findByText("test message 3")).toBeInTheDocument();

  ws_server.close();
  WS.clean();
});
