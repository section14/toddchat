import { useState } from "react";
import ChatWindow from "./ChatWindow";
import UserList from "./user/UserList";

const ChatDashboard = () => {
  interface Room {
    id: number;
    hidden: boolean;
  }

  //const [rooms, addRoom] = useState<number[]>([]);
  const [rooms, addRoom] = useState<Room[]>([]);

  const launchChat = (id: number) => {
    //make sure room doesn't exist already
    const exists = rooms.find((room) => room.id === id);

    //add a new room
    if (!exists) {
      addRoom([...rooms, { id: id, hidden: false }]);
    }

    //set remaining chat windows hidden
    for (let i = 0; i < rooms.length; i++) {
      if (rooms[i].id !== id) {
        rooms[i].hidden = true;
      } else {
        rooms[i].hidden = false;
      }
    }

		//cause a re-render to update UI
		addRoom(rooms => [...rooms])
  };

  return (
    <>
      <div className="row vh-100">
        <div className="col-md-3 bg-light-grey-1">
          <UserList callback={launchChat} />
        </div>

        <div className="col-md-7">
          {rooms.map((room, i) => {
            return (
              <div key={i}>
                <ChatWindow user={room.id} hidden={room.hidden} />
              </div>
            );
          })}
        </div>

        <div className="col-md-2"></div>
      </div>
    </>
  );
};

export default ChatDashboard;
