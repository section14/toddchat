//create a web socket
export const connect = (addr: string): WebSocket => {
  //create new web socket
  const ws = new WebSocket(addr);

  return ws;
};

//create instance
//todo: make this address an argument that's only
//used once
export const makeConn = (): WebSocket => {
  const ws = connect("ws://192.168.0.24:8008/chat");
  return ws;
};

//single web socket instance
export const socketConn: WebSocket = makeConn();
