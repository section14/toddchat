import { useState, useEffect, useRef } from "react";
import { Message } from "./message/MessageList";
import MessageList from "./message/MessageList";
import MessageEntry from "./message/MessageEntry";
import { getAuthData, getChatToken, api, socket } from "./request/http";

const ChatWindow = (props: { user: number; hidden: boolean }) => {
  const [userId, setUserId] = useState(0);
  const [socketOpen, setSocket] = useState(false);
  const [messages, updateMessages] = useState<Message[]>([]);

  //get a reference to global web socket created onload
  const ws = useRef<WebSocket | null>(null);

  //start up
  useEffect(() => {
    startSocket();

    return () => {
      ws.current!.close();
    };
  }, []);

  const startSocket = async () => {
    console.log("chat started");

    const token = await createChatToken().catch((e: any) => {
      console.log("error creating chat token: ", e.message);
    });

    if (token) {
      ws.current = new WebSocket(
        `${socket()}/chat/${token}/user/${props.user}`
      );
      ws.current.onopen = () => {
				console.log("socket opened");
				setSocket(true)
			}
      ws.current.onclose = () => {
        console.log("socket closed");
				setSocket(false)
        setTimeout(() => startSocket(), 5000);
      };
    }
  };

  useEffect(() => {
    const getPreviousMessages = async () => {
      const token = localStorage.getItem("token");

      const res = await getAuthData(
        `${api()}/messages/user/${props.user}`,
        token!
      ).catch((e: any) => {
        console.log("error getting messages: ", e.message);
      });

      if (res) {
        //update messages on launch
        addPrevMessages(res);
      }
    };

    getPreviousMessages();
  }, [userId]);

  //add previous chat session messages
  const addPrevMessages = (msgArr: any) => {
    let msgs = msgArr.map(
      (msg: any) =>
        ({
          content: msg.value,
          time: msg.timestamp,
          me: sentByMe(msg.user_id), //compare user id's here
        } as Message)
    );

    //reverse list so the last message appears first
    msgs.reverse();

    updateMessages(msgs);
  };

  //watch socket messages
  useEffect(() => {
    if (socketOpen) {
      ws.current!.onmessage = (event: any) => {
        captureMessage(event.data);
      };
    }
  }, [socketOpen]);

  //captures message from websocket
  const captureMessage = (data: any) => {
    const json = JSON.parse(data);

    const newMessage: Message = {
      content: json.msg,
      time: "12000",
      me: sentByMe(json.user_id),
    };
    updateMessages((messages) => [...messages, newMessage]);
  };

  //callback function to capture messages being sent
  const addMessage = (msg: string) => {
    ws.current!.send(msg);
  };

  //format message (color, alignment) based on whether
  //or not the current user sent it
  const sentByMe = (id: number): boolean => {
    if (id === userId) {
      return true;
    }

    return false;
  };

  //create token to verify initiated chat with api
  const createChatToken = async () => {
    const token = localStorage.getItem("token");
    const res = await getChatToken(`${api()}/chat/token/create`, token!).catch(
      (e: any) => {
        console.log("bad chat token", e.message);
        return "";
      }
    );

    if (res !== null || res !== undefined) {
      setUserId(res.user_id);
      setSocket(true);
      return res.chat_token;
    }

    return "";
  };

  return (
    <>
      <div className={`row ${props.hidden ? "hidden" : ""}`}>
        <div className="col">
          <div className="mt-5">
            <MessageList messages={messages} />
          </div>

          <div className="mt-5">
            <MessageEntry callback={addMessage} />
          </div>
        </div>
      </div>
    </>
  );
};

export default ChatWindow;
