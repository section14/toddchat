# go backend ###########################################

FROM golang:1.16.5-alpine3.13

#RUN mkdir /app/backend
WORKDIR /app

EXPOSE 8008

COPY . .
COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY cmd/chat/config.json ./

RUN go build -o serv cmd/chat/main.go

CMD ["./serv"]

