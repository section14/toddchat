package config

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strconv"
)

type config struct {
	Location string   `json:"location"`
	Origin   []string `json:"origin"`
	Port     string   `json:"port"`
	Logs     string   `json:"logs"`
	DbHost   string   `json:"db_host"`
	DbPort   string   `json:"db_port"`
	DbName   string   `json:"db_name"`
}

type Config interface {
	Build(configPath string) error
	GetDb() *sql.DB
	GetOrigin() []string
	//WriteLog(logType logger.LogType, msg string)
	//WriteError(logType logger.LogType, msg string, err error)
	GetPort() string
}

type Env struct {
	Db       *sql.DB
	Origin   []string
	Location string
	Logs     string
	//LogType  logger.LogType
	Port string
}

//private vars
var dbhost string
var dbpassword string
var dbport int
var tokenSalt string

const dbuser = "postgres"

//get salt string for jwt tokens
func GetSalt() []byte {
	return []byte(tokenSalt)
}

func (env *Env) GetDb() *sql.DB {
	return env.Db
}

/*func (env *Env) WriteLog(logType logger.LogType, msg string) {
	logger.WriteLog(logType, msg, env.Logs)
}

func (env *Env) WriteError(logType logger.LogType, msg string, err error) {
	logger.WriteError(logType, env.Logs, msg, err)
}
*/

func (env *Env) GetOrigin() []string {
	return env.Origin
}

func (env *Env) GetPort() string {
	return env.Port
}

//this reads in values from a json file and proccesses
//or assigns those values to globally available vars
func (env *Env) Build(configPath string) error {
	file, err := os.Open(configPath)
	if err != nil {
		log.Println(err)
	}

	decoder := json.NewDecoder(file)

	var conf config
	err = decoder.Decode(&conf)
	if err != nil {
		log.Println(err)
	}

	//populate global variables
	env.Location = conf.Location
	env.Logs = conf.Logs
	env.Port = conf.Port
	env.Origin = conf.Origin

	dbhost = conf.DbHost
	dbport, _ = strconv.Atoi(conf.DbPort)

	//set password from environment variable. this can
	//be changed to whichever method you prefer
	val, ok := os.LookupEnv("DB_PASS")
	if !ok {
		fmt.Println("DB_PASS env var not set")
	}

	dbpassword = val

	//connect to db, and set struct value
	env.Db = connection(conf.DbName)

	//setup connection limits
	env.Db.SetMaxOpenConns(20)
	env.Db.SetMaxIdleConns(2)

	val, ok = os.LookupEnv("TOKEN_SALT")
	if !ok {
		fmt.Println("TOKEN env var not set")
	}

	tokenSalt = val

	file.Close()

	return nil
}

func connection(dbName string) *sql.DB {
	//make connection to postgres db with a specified database name
	conn := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", dbhost, dbport, dbuser, dbpassword, dbName) //sslmode=verify-full
	db, err := sql.Open("postgres", conn)
	if err != nil {
		panic(err.Error())
	}

	//verify connection
	err = db.Ping()
	if err != nil {
		panic(err.Error())
	}

	return db
}
