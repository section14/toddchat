package messagetest

import (
	"time"

	"gitlab.com/section14/toddchat/internal/config"
	"gitlab.com/section14/toddchat/internal/message"
)

type MessageMock struct{}

func (m *MessageMock) GetRoomMessages(id int, env config.Config) []message.Message {
	messages := []message.Message{
		{
			Id:        1,
			Value:     "test",
			UserId:    1,
			RoomId:    1,
			Timestamp: time.Now(),
		},

		{
			Id:        2,
			Value:     "test part two",
			UserId:    2,
			RoomId:    1,
			Timestamp: time.Now(),
		},
	}

	return messages
}

func (m *MessageMock) GetRoomMessagesByKey(id int, connectUserId int, env config.Config) []message.Message {
	messages := []message.Message{
		{
			Id:        1,
			Value:     "test",
			UserId:    1,
			RoomId:    2,
			Timestamp: time.Now(),
		},

		{
			Id:        2,
			Value:     "test part two",
			UserId:    2,
			RoomId:    2,
			Timestamp: time.Now(),
		},
	}

	return messages
}

func (m *MessageMock) Send(env config.Config) {}

type RoomMock struct{}

func (r *RoomMock) GetRoomId(userId, connectUserId int, env config.Config) int {
	return 1
}

func (r *RoomMock) CreateRoom(userId, connectUserId int, env config.Config) (int, error) {
	return 1, nil
}
