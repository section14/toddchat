package message

import (
	"log"
	"time"

	"gitlab.com/section14/toddchat/internal/config"
)

type Message struct {
	Id        int       `json:"id"`
	Value     string    `json:"value"`
	UserId    int       `json:"user_id"`
	RoomId    int       `json:"room_id"`
	Timestamp time.Time `json:"timestamp"`
}

type MessageService interface {
	Send(env config.Config)
	GetRoomMessages(id int, env config.Config) []Message
	GetRoomMessagesByKey(id int, connectUserId int, env config.Config) []Message
}

//insert a sent message into the database
func (m *Message) Send(env config.Config) {
	db := env.GetDb()

	q := `INSERT INTO messages(value,users_id,room_id,timestamp) VALUES($1,$2,$3,$4)`
	stmt, err := db.Prepare(q)
	if err != nil {
		log.Println(err)
	}
	defer stmt.Close()

	_, err = stmt.Exec(m.Value, m.UserId, m.RoomId, time.Now())
	if err != nil {
		log.Println(err)
	}
}

//return messages based on room id
//todo: limit the number of messages
//probably need to keep track of the range
//on the frontend somehow
func (m *Message) GetRoomMessages(id int, env config.Config) []Message {
	db := env.GetDb()
	var messages []Message

	q := `SELECT value,users_id FROM messages WHERE id=$1`
	stmt, err := db.Prepare(q)
	if err != nil {
		log.Println(err)
	}
	defer stmt.Close()

	rows, err := stmt.Query(id)
	if err != nil {
		log.Println(err)
	}
	defer rows.Close()

	for rows.Next() {
		var message Message

		rows.Scan(&message.Value, &message.UserId)
		messages = append(messages, message)
	}

	return messages
}

func (m *Message) GetRoomMessagesByKey(id int, connectUserId int, env config.Config) []Message {
	db := env.GetDb()
	var messages []Message

	//get room based on id of two users
	var room Room
	roomId := room.GetRoomId(id, connectUserId, env)

	q := `SELECT value,users_id FROM messages WHERE room_id=$1 ORDER BY id DESC LIMIT 20`
	stmt, err := db.Prepare(q)
	if err != nil {
		log.Println(err)
	}
	defer stmt.Close()

	rows, err := stmt.Query(roomId)
	if err != nil {
		log.Println(err)
	}
	defer rows.Close()

	for rows.Next() {
		var message Message

		rows.Scan(&message.Value, &message.UserId)
		messages = append(messages, message)
	}

	return messages
}
