package message

import (
	"database/sql"
	"fmt"
	"log"

	"gitlab.com/section14/toddchat/internal/config"
	"gitlab.com/section14/toddchat/internal/user"
)

type Room struct {
	Id   int
	Name string
	Key  string
}

type RoomService interface {
	GetRoomId(userId, connectUserId int, env config.Config) int
	CreateRoom(userId, connectUserId int, env config.Config) (int, error)
}

//get a room id based on a logged in user id
//and a requested user id
func (r *Room) GetRoomId(userId, connectUserId int, env config.Config) int {
	db := env.GetDb()

	//create key from concactonated id's
	//id's are in ascending order
	key := createKey(userId, connectUserId)

	q := `SELECT id FROM room WHERE key=$1`
	stmt, err := db.Prepare(q)
	if err != nil {
		log.Println(err)
	}
	defer stmt.Close()

	var id int
	err = stmt.QueryRow(key).Scan(&id)
	if err != nil {
		//no results, create a new room
		if err == sql.ErrNoRows {
			id, err = r.CreateRoom(userId, connectUserId, env)
			if err != nil {
				fmt.Println(err)
			}

			return id
		}

		log.Println(err)
	}

	return id
}

//create a room based on two user id's
func (r *Room) CreateRoom(userId, connectUserId int, env config.Config) (int, error) {
	db := env.GetDb()

	//create key from concactonated id's
	//id's are in ascending order
	key := createKey(userId, connectUserId)

	//get users and create a room name
	var user1 user.User
	err := user1.GetUser(userId, env)
	if err != nil {
		log.Println(err)
	}

	var user2 user.User
	err = user2.GetUser(connectUserId, env)
	if err != nil {
		log.Println(err)
	}

	roomName := fmt.Sprintf("%s-%s", user1.Name, user2.Name)

	q := `INSERT INTO room(name,key) VALUES($1,$2) RETURNING id`
	stmt, err := db.Prepare(q)
	if err != nil {
		log.Println(err)
	}

	id := 0
	err = stmt.QueryRow(roomName, key).Scan(&id)
	if err != nil {
		log.Println(err)
	}

	return id, nil
}

//create a string from two ints
func createKey(a, b int) string {
	if a < b {
		return fmt.Sprintf("%d-%d", a, b)
	}

	return fmt.Sprintf("%d-%d", b, a)
}
