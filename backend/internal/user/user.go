package user

import (
	"errors"

	"gitlab.com/section14/toddchat/internal/config"
)

type User struct {
	Id       int    `json:"id"`
	Name     string `json:"name"`
	Password string `json:"password"`
}

type UserService interface {
	GetOtherUsers(id int, env config.Config) ([]User, error)
	GetUserFromName(name string, env config.Config) error
	GetUser(id int, env config.Config) error
	GetPassword() string
	GetId() int
}

//get users who aren't the supplied user
func (u *User) GetOtherUsers(id int, env config.Config) ([]User, error) {
	db := env.GetDb()
	var users []User

	sql := "SELECT id,name FROM users WHERE NOT id=$1"
	stmt, err := db.Prepare(sql)
	if err != nil {
		return users, err
	}
	defer stmt.Close()

	rows, err := stmt.Query(id)
	defer rows.Close()

	for rows.Next() {
		var user User

		rows.Scan(&user.Id, &user.Name)
		users = append(users, user)
	}

	return users, nil
}

//get user by username
func (u *User) GetUserFromName(name string, env config.Config) error {
	db := env.GetDb()
	sql := "SELECT id,password FROM users WHERE name=$1"
	stmt, err := db.Prepare(sql)
	if err != nil {
		return errors.New("error logging user in")
	}
	defer stmt.Close()

	u.Name = name
	err = stmt.QueryRow(name).Scan(&u.Id, &u.Password)
	if err != nil {
		return errors.New("error logging user in")
	}

	return nil
}

//get user based on id
func (u *User) GetUser(id int, env config.Config) error {
	db := env.GetDb()
	sql := "SELECT name FROM users WHERE id=$1"
	stmt, err := db.Prepare(sql)
	if err != nil {
		return errors.New("error getting user name")
	}
	defer stmt.Close()

	err = stmt.QueryRow(id).Scan(&u.Name)
	if err != nil {
		return errors.New("error getting username")
	}

	return nil
}

func (u *User) GetPassword() string {
	return u.Password
}

func (u *User) GetId() int {
	return u.Id
}
