package usertest

import (
	"gitlab.com/section14/toddchat/internal/config"
	"gitlab.com/section14/toddchat/internal/user"
	"golang.org/x/crypto/bcrypt"
)

type UserMock struct{}

func (u *UserMock) GetOtherUsers(id int, env config.Config) ([]user.User, error) {
	users := []user.User{
		{
			Id:       1,
			Name:     "Todd",
			Password: "123456!",
		},
		{
			Id:       2,
			Name:     "Jenny",
			Password: "789012!",
		},
	}

	return users, nil
}

func (u *UserMock) GetUserFromName(name string, env config.Config) error {
	return nil
}

func (u *UserMock) GetUser(id int, env config.Config) error {
	return nil
}

func (u *UserMock) GetPassword() string {
	pass := "testpass"

	encoded, err := bcrypt.GenerateFromPassword([]byte(pass), 10)
	if err != nil {
		return "error"
	}

	return string(encoded)
}

func (u *UserMock) GetId() int {
	return 1
}
