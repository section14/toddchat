package authtest

//utility for auth mocking

import (
	"errors"
	"net/http"

	"github.com/golang-jwt/jwt"
	"gitlab.com/section14/toddchat/internal/config"
	"gitlab.com/section14/toddchat/internal/user"
)

type AuthMock struct{}

func (a *AuthMock) LoginUser(r *http.Request, u user.UserService, env config.Config) (string, error) {
	return "abc123", nil
}

func (a *AuthMock) VerifyUser(handler http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("ok"))
	}
}

func (a *AuthMock) CreateChatToken(w http.ResponseWriter, r *http.Request) (int, string, error) {
	return 1, "fake_token", nil
}

func (a *AuthMock) VerifyChat(token string) (int, error) {
	return 1, nil
}

func (a *AuthMock) GetUserId(w http.ResponseWriter, r *http.Request) int {
	return 1
}

//token mocking
type TokenMock struct{}

func (t *TokenMock) VerifyToken(myToken string) (int, error) {
	if myToken != "test123" {
		return 0, errors.New("supplied token should be test123")
	}

	return 1, nil
}

func (t *TokenMock) GenerateToken(u user.UserService) (string, error) {
	return "test123", nil
}

func (t *TokenMock) ParseToken(myToken string) (*jwt.Token, error) {
	return &jwt.Token{}, nil
}
