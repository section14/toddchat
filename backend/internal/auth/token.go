package auth

import (
	"errors"
	"fmt"
	"time"

	"github.com/golang-jwt/jwt"
	"gitlab.com/section14/toddchat/internal/config"
	"gitlab.com/section14/toddchat/internal/user"
)

type TokenService interface {
	VerifyToken(myToken string) (int, error)
	GenerateToken(u user.UserService) (string, error)
	ParseToken(myToken string) (*jwt.Token, error)
}

type Token struct{}

//get salt string from environment var in config
var signer []byte = config.GetSalt()

//verify token and return user id on success
//
//this should come from a http.Request.Header.Get()
//using whatever header name it was sent by
func (t *Token) VerifyToken(myToken string) (int, error) {
	token, err := t.ParseToken(myToken)
	if err != nil {
		return 0, err
	}

	//get claims from parsed token
	claims := token.Claims.(jwt.MapClaims)
	userId := claims["userId"].(float64)
	exp := claims["exp"].(float64)

	//verify token is not expired
	if time.Now().Unix() > int64(exp) {
		return 0, errors.New("token expired")
	}

	return int(userId), nil
}

//generate a new jwt token
func (t *Token) GenerateToken(u user.UserService) (string, error) {
	token := jwt.New(jwt.SigningMethodHS256)
	claims := token.Claims.(jwt.MapClaims)

	claims["userId"] = u.GetId()
	claims["exp"] = time.Now().Add(time.Hour * 48).Unix()

	tokenStr, err := token.SignedString(signer)
	if err != nil {
		return "", err
	}

	return tokenStr, nil
}

//parse jwt token
func (t *Token) ParseToken(myToken string) (*jwt.Token, error) {
	token, err := jwt.Parse(myToken, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}

		return signer, nil
	})

	if token.Valid {
		return token, nil
	}

	return nil, err
}
