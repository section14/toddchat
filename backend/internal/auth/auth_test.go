package auth

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/section14/toddchat/internal/auth/authtest"
	"gitlab.com/section14/toddchat/internal/config"
	"gitlab.com/section14/toddchat/internal/user/usertest"
)

func TestLoginUser(t *testing.T) {
	service := &usertest.UserMock{}
	env := &config.Env{}

	type loginCreds struct {
		Username string `json:"username"`
		Password string `json:"password"`
	}

	t.Run("request with correct data", func(t *testing.T) {
		//send login data with mocked request
		lc := loginCreds{Username: "test", Password: "testpass"}

		jsonData, err := json.Marshal(lc)
		if err != nil {
			t.Error("JSON error: ", err)
		}

		req := httptest.NewRequest(http.MethodPost, "/api/login", bytes.NewBuffer(jsonData))

		//attempt login
		a := &Auth{Token: &authtest.TokenMock{}}
		jwtToken, err := a.LoginUser(req, service, env)

		if jwtToken == "" {
			t.Error("jwt token from LoginUser should not be empty")
		}

		if err != nil {
			t.Error("Login error: ", err)
		}
	})

	t.Run("request with password missing", func(t *testing.T) {
		//send login data with mocked request
		lc := loginCreds{Username: "test", Password: ""}

		jsonData, err := json.Marshal(lc)
		if err != nil {
			t.Error("JSON error: ", err)
		}

		req := httptest.NewRequest(http.MethodPost, "/api/login", bytes.NewBuffer(jsonData))

		//attempt login
		a := &Auth{Token: &authtest.TokenMock{}}
		jwtToken, err := a.LoginUser(req, service, env)

		if jwtToken != "" {
			t.Error("jwt token shoud be empty on a bad request")
		}

		if err == nil {
			t.Error("logins with empty passwords should return an error")
		}
	})
}

func TestVerifyUser(t *testing.T) {
	//test request and response
	req := httptest.NewRequest(http.MethodPost, "/api/login", nil)
	req.Header["User-Token"] = []string{"test123"}
	res := httptest.NewRecorder()

	testHandle := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		//do something
		w.Write([]byte("oi selecta"))
	})

	a := &Auth{Token: &authtest.TokenMock{}}
	testVerify := a.VerifyUser(testHandle)

	//test serve
	testVerify.ServeHTTP(res, req)
}

//this tests auth.GetUserId also
func TestCreateChatToken(t *testing.T) {
	//test request and response
	req := httptest.NewRequest(http.MethodPost, "/api/chat/token/create", nil)
	req.Header["User-Token"] = []string{"test123"}
	res := httptest.NewRecorder()

	a := &Auth{Token: &authtest.TokenMock{}}

	id, token, err := a.CreateChatToken(res, req)

	if id != 1 {
		t.Errorf("id from CreateChatToken should be 1, got: %d", id)
	}

	if token != "test123" {
		t.Errorf("token from CreateChatToken should be \"test123\", got: %s ", token)
	}

	if err != nil {
		t.Error(err)
	}
}

func TestVerifyChat(t *testing.T) {
	a := &Auth{Token: &authtest.TokenMock{}}

	id, err := a.VerifyChat("test123")

	if id != 1 {
		t.Errorf("id from VerifyChat should be 1, got %d", id)
	}

	if err != nil {
		t.Error(err)
	}
}
