package auth

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	"gitlab.com/section14/toddchat/internal/config"
	"gitlab.com/section14/toddchat/internal/user"
	"golang.org/x/crypto/bcrypt"
)

const TOKEN_HEADER = "User-Token"

type AuthService interface {
	LoginUser(r *http.Request, u user.UserService, env config.Config) (string, error)
	VerifyUser(handler http.HandlerFunc) http.HandlerFunc
	CreateChatToken(w http.ResponseWriter, r *http.Request) (int, string, error)
	VerifyChat(token string) (int, error)
	GetUserId(w http.ResponseWriter, r *http.Request) int
}

type Auth struct {
	Token TokenService
}

//log user in via an http post request. return a valid
//jwt token if successful
func (a *Auth) LoginUser(r *http.Request, u user.UserService, env config.Config) (string, error) {
	type loginVal struct {
		Username string `json:"username"`
		Password string `json:"password"`
	}

	var lv loginVal

	decoder := json.NewDecoder(r.Body)
	decoder.Decode(&lv)

	if lv.Username == "" || lv.Password == "" {
		return "", errors.New("username or password not set")
	}

	//make database request
	err := u.GetUserFromName(lv.Username, env)

	//verify passwords - nil signals a match
	compared := bcrypt.CompareHashAndPassword([]byte(u.GetPassword()), []byte(lv.Password))
	if compared != nil {
		return "", errors.New("passwords don't match")
	}

	//generate a jwt token to return
	token, err := a.Token.GenerateToken(u)
	if err != nil {
		return "", err
	}

	return token, nil
}

//middleware function to verify users are logged in
func (a *Auth) VerifyUser(handler http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		//parse and verify jwt token
		token := r.Header.Get(TOKEN_HEADER)

		//empty token, return
		if token == "" {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}

		_, err := a.Token.VerifyToken(token)

		if err != nil {
			//log error
			fmt.Println(err)
			w.WriteHeader(http.StatusUnauthorized)
			return
		}

		handler(w, r)
	}
}

func (a *Auth) CreateChatToken(w http.ResponseWriter, r *http.Request) (int, string, error) {
	id := a.GetUserId(w, r)

	//0 == bad request
	if id == 0 {
		//todo: this needs to make more sense
		return 0, "", errors.New("chat token could not get user id")
	}

	u := &user.User{Id: id}

	//generate a jwt token to return
	token, err := a.Token.GenerateToken(u)
	if err != nil {
		return 0, "", err
	}

	return id, token, nil
}

//verify a user to a chat session
func (a *Auth) VerifyChat(token string) (int, error) {
	//empty token, return
	if token == "" {
		return 0, errors.New("cannot verify empty token")
	}

	id, err := a.Token.VerifyToken(token)

	if err != nil {
		return 0, err
	}

	return id, nil
}

//get user id from jwt token
func (a *Auth) GetUserId(w http.ResponseWriter, r *http.Request) int {
	token := r.Header.Get(TOKEN_HEADER)
	userId, err := a.Token.VerifyToken(token)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return 0
	}

	return userId
}

//todo: this makes sense as an auth function
//but should it go in user package?
//add new user
func AddUser(name string, pass string, env config.Config) error {
	db := env.GetDb()

	//encrypt password
	password, err := encodePassword(pass)
	if err != nil {
		return err
	}

	sql := "INSERT INTO users(name,password) VALUES($1,$2)"
	stmt, err := db.Prepare(sql)
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.Exec(name, password)
	if err != nil {
		return err
	}

	return nil
}

func encodePassword(s string) (string, error) {
	encoded, err := bcrypt.GenerateFromPassword([]byte(s), 10)
	if err != nil {
		return "", err
	}

	return string(encoded), nil
}
