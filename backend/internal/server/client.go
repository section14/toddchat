package server

import (
	"bytes"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/websocket"
	"gitlab.com/section14/toddchat/internal/config"
	"gitlab.com/section14/toddchat/internal/message"
)

const (
	//time wait for reads
	writeWait = 10 * time.Second

	//time to wait for writes
	readWait = 60 * time.Second

	pingTime   = (writeWait * 9) / 10
	maxMsgSize = 512
)

var (
	newline = []byte{'\n'}
	space   = []byte{' '}
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin:     func(r *http.Request) bool { return true },
}

type Client struct {
	hub    *Hub
	conn   *websocket.Conn
	send   chan []byte
	userId int
	roomId int
}

type ClientMessage struct {
	UserId  int    `json:"user_id"`
	RoomId  int    `json:"room_id"`
	Message string `json:"msg"`
}

//read message from websocket connection
//this comes from a frontend client
func (c *Client) ReadMessage() {
	//defer unregistering a client, and closing websocket
	//connection, until exiting function
	defer func() {
		c.hub.unregister <- c
		c.conn.Close()
	}()

	//set connection read parameters
	c.conn.SetReadLimit(maxMsgSize)
	c.conn.SetReadDeadline(time.Now().Add(readWait))
	c.conn.SetPongHandler(func(string) error { c.conn.SetReadDeadline(time.Now().Add(readWait)); return nil })

	//continuiously read messages
	for {
		_, msg, err := c.conn.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				log.Println(err)
			}
			break
		}

		//trim any whitespace from the message
		msg = bytes.TrimSpace(bytes.Replace(msg, newline, space, -1))

		//if message doesn't contain any data, reject it
		if len(msg) == 0 {
			continue
		}

		//create a goroutine here to write a new message to the database
		m := message.Message{Value: string(msg), UserId: c.userId, RoomId: c.roomId}
		go m.Send(c.hub.env)

		//
		clientMessage := ClientMessage{
			UserId:  c.userId,
			RoomId:  c.roomId,
			Message: string(msg)}

		//msg, _ = json.Marshal(clientMessage)

		///write to broadcast channel
		c.hub.broadcast <- &clientMessage
	}
}

//write message to web socket
//this writes to all listening clients
func (c *Client) WriteMessage() {
	ticker := time.NewTicker(pingTime)

	defer func() {
		ticker.Stop()
		c.conn.Close()
	}()

	for {
		select {
		case message, ok := <-c.send:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))

			//channel is already closed, so exit
			if !ok {
				c.conn.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}

			//write message to next available writer
			w, err := c.conn.NextWriter(websocket.TextMessage)
			if err != nil {
				return
			}
			w.Write(message)

			//add queued messages to web socket -- figure out what's going on here
			size := len(c.send)

			for i := 0; i < size; i++ {
				w.Write(newline)
				w.Write(<-c.send)
			}

			//this flushes the write buffer and closes the writer
			if err := w.Close(); err != nil {
				return
			}

		case <-ticker.C:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := c.conn.WriteMessage(websocket.PingMessage, nil); err != nil {
				return
			}

		}
	}
}

//this is the main entry point for clients (js frontends, apps, etc)
func ServeWebSocket(hub *Hub, userId int, connectUserId int, env config.Config, w http.ResponseWriter, r *http.Request) {
	headers := r.Header

	for key, val := range headers {
		fmt.Printf("%s :: %s\n", key, val)
	}

	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}

	//get chat room id
	var room message.Room
	roomId := room.GetRoomId(userId, connectUserId, env)

	client := &Client{hub: hub,
		conn:   conn,
		send:   make(chan []byte, 256),
		userId: userId,
		roomId: roomId}

	client.hub.register <- client

	//go client.timerQuit()

	//read and write in goroutines
	go client.ReadMessage()
	go client.WriteMessage()
}
