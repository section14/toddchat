package server

import (
	"encoding/json"
	"fmt"
	"time"

	"gitlab.com/section14/toddchat/internal/config"
)

type Hub struct {
	//registered clients
	clients map[*Client]bool

	//incoming messages
	broadcast chan *ClientMessage

	//client register requests
	register chan *Client

	//client unregister requests
	unregister chan *Client

	//env variables
	env config.Config
}

//function to return an initialized Hub
func CreateHub(env config.Config) *Hub {
	return &Hub{
		clients:    make(map[*Client]bool),
		broadcast:  make(chan *ClientMessage),
		register:   make(chan *Client),
		unregister: make(chan *Client),
		env:        env,
	}
}

//run our hub and listen to each channel
func (h *Hub) Run() {
	for {
		select {
		case client := <-h.register:
			h.clients[client] = true

		case client := <-h.unregister:
			if _, ok := h.clients[client]; ok {
				delete(h.clients, client)
				close(client.send)
			}

		case message := <-h.broadcast:
			for client := range h.clients {
				//only broadcast to clients with matching
				//room id
				if message.RoomId == client.roomId {

					msg, _ := json.Marshal(message)

					select {
					case client.send <- msg:
					default:
						close(client.send)
						delete(h.clients, client)
					}
				}
			}
		}
	}
}

func (c *Client) timerQuit() {
	timer := time.NewTimer(60 * time.Second)

	<-timer.C
	c.hub.unregister <- c
	fmt.Println("quit web socket")
}
