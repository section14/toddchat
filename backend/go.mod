module gitlab.com/section14/toddchat

go 1.16

require (
	github.com/golang-jwt/jwt v3.2.1+incompatible
	github.com/gorilla/handlers v1.4.2
	github.com/gorilla/mux v1.7.4
	github.com/gorilla/websocket v1.4.2
	github.com/lib/pq v1.10.2
	golang.org/x/crypto v0.0.0-20210711020723-a769d52b0f97
)
