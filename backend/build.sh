#!/bin/sh

set -e
echo "building backend"
cd src/cmd/chat
go build -o chat main.go
mv chat $BUILDPATH
echo "starting backend"
cd $BUILDPATH
./chat
