package main

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/section14/toddchat/internal/auth/authtest"
	"gitlab.com/section14/toddchat/internal/config"
	"gitlab.com/section14/toddchat/internal/message"
	"gitlab.com/section14/toddchat/internal/message/messagetest"
	"gitlab.com/section14/toddchat/internal/user"
	"gitlab.com/section14/toddchat/internal/user/usertest"
)

//user mocks

//message mocks
func TestLoginUser(t *testing.T) {
	auth := &authtest.AuthMock{}
	service := &usertest.UserMock{}
	env := &config.Env{}
	userServer := NewUserServer(service, auth, env)

	req := httptest.NewRequest(http.MethodPost, "/api/login", nil)
	res := httptest.NewRecorder()

	userServer.LoginHandler(res, req)

	type Body struct {
		Token string `json:"token"`
	}

	var body Body

	decoder := json.NewDecoder(res.Body)
	decoder.Decode(&body)

	if body.Token != "abc123" {
		t.Errorf("token from login should be \"abc123\", got: %s", body.Token)
	}
}

//todo: figure out how to test for websocket connection
func TestChatHandler(t *testing.T) {
	auth := &authtest.AuthMock{}
	service := &messagetest.MessageMock{}
	env := &config.Env{}
	messageServer := NewMessageServer(service, auth, env)

	req := httptest.NewRequest(http.MethodGet, "/api/chat/abc123/user/1", nil)
	res := httptest.NewRecorder()

	messageServer.ChatHandler(res, req)
}

func TestCreateChatToken(t *testing.T) {
	auth := &authtest.AuthMock{}
	service := &usertest.UserMock{}
	env := &config.Env{}
	userServer := NewUserServer(service, auth, env)

	req := httptest.NewRequest(http.MethodGet, "/api/chat/token/create", nil)
	res := httptest.NewRecorder()

	userServer.ChatTokenHandler(res, req)

	//decode response
	type Body struct {
		UserId    int    `json:"user_id"`
		ChatToken string `json:"chat_token"`
	}

	var body Body

	decoder := json.NewDecoder(res.Body)
	decoder.Decode(&body)

	if body.UserId != 1 {
		t.Error("user_id in response body should be 1")
	}

	if body.ChatToken != "fake_token" {
		t.Error("chat_token in response body should be 'fake_token'")
	}
}

func TestGetNotUsers(t *testing.T) {
	auth := &authtest.AuthMock{}
	service := &usertest.UserMock{}
	env := &config.Env{}
	userServer := NewUserServer(service, auth, env)

	req := httptest.NewRequest(http.MethodGet, "/api/user/get/not", nil)
	res := httptest.NewRecorder()

	userServer.UsersNotHandler(res, req)

	//decode response
	var body []user.User

	decoder := json.NewDecoder(res.Body)
	decoder.Decode(&body)

	if len(body) != 2 {
		t.Errorf("response should be an array with two items, got %d", len(body))
	}

	if body[0].Name != "Todd" {
		t.Errorf("Name on first entry should be Todd, got: %s", body[0].Name)
	}

	if body[1].Id != 2 {
		t.Errorf("Id on second entry should be 2, got: %d", body[1].Id)
	}
}

//todo: this, and TestGetRoomMessagesByKey need looked at again
func TestGetRoomMessages(t *testing.T) {
	auth := &authtest.AuthMock{}
	service := &messagetest.MessageMock{}
	env := &config.Env{}
	messageServer := NewMessageServer(service, auth, env)

	req := httptest.NewRequest(http.MethodGet, "/api/messages/user/1", nil)
	res := httptest.NewRecorder()

	messageServer.RoomMessagesHandler(res, req)

	//decode response
	var body []message.Message

	decoder := json.NewDecoder(res.Body)
	decoder.Decode(&body)

	if len(body) != 2 {
		t.Errorf("response should be an array with two items, got %d", len(body))
	}

	if body[0].RoomId != 2 {
		t.Errorf("room id of first message should be 2, got: %d", body[0].RoomId)
	}

	if body[1].Value != "test part two" {
		t.Errorf("value of second message should be \"test part two\", got: %s", body[1].Value)
	}
}

func TestGetRoomMessagesByKey(t *testing.T) {
	auth := &authtest.AuthMock{}
	service := &messagetest.MessageMock{}
	env := &config.Env{}
	messageServer := NewMessageServer(service, auth, env)

	req := httptest.NewRequest(http.MethodGet, "/api/messages/user/1", nil)
	res := httptest.NewRecorder()

	messageServer.RoomMessagesHandler(res, req)

	//decode response
	var body []message.Message

	decoder := json.NewDecoder(res.Body)
	decoder.Decode(&body)

	if len(body) != 2 {
		t.Errorf("response should be an array with two items, got %d", len(body))
	}

	if body[0].RoomId != 2 {
		t.Errorf("room id of first message should be 1, got: %d", body[0].RoomId)
	}

	if body[0].Value != "test" {
		t.Errorf("value of first message should be \"test\", got: %s", body[1].Value)
	}
}
