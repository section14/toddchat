package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	_ "github.com/lib/pq"

	"gitlab.com/section14/toddchat/internal/auth"
	"gitlab.com/section14/toddchat/internal/config"
	"gitlab.com/section14/toddchat/internal/message"
	"gitlab.com/section14/toddchat/internal/server"
	"gitlab.com/section14/toddchat/internal/user"
)

type AuthServer struct {
	service auth.AuthService
	env     config.Config
}

type UserServer struct {
	service user.UserService
	auth    auth.AuthService
	env     config.Config
}

type MessageServer struct {
	service message.MessageService
	auth    auth.AuthService
	env     config.Config
}

var hub *server.Hub

func main() {
	//get config location from env variable
	cfg, ok := os.LookupEnv("CONFIG")
	if !ok {
		fmt.Println("CONFIG not set in env")
	}

	//build app config
	env := &config.Env{}
	env.Build(cfg)

	//create websocket server
	hub = server.CreateHub(env)
	go hub.Run()

	r := mux.NewRouter()

	//headers
	headersOk := handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type", "User-Token"})
	methodsOk := handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "OPTIONS"})

	//cors handling
	//originsOk := handlers.AllowedOrigins([]string{"*"})
	originsOk := handlers.AllowedOrigins(env.GetOrigin())

	routes(r, env)

	//set up listener
	l, err := net.Listen("tcp4", env.GetPort())
	if err != nil {
		log.Fatal(err)
	}

	//config server
	server := &http.Server{
		Handler:      handlers.CORS(originsOk, headersOk, methodsOk)(r),
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 60 * time.Second,
	}

	//launch server
	log.Fatal(server.Serve(l))
}

func NewAuthServer(service auth.AuthService, env config.Config) *AuthServer {
	return &AuthServer{service: service, env: env}
}

func NewUserServer(service user.UserService, auth auth.AuthService, env config.Config) *UserServer {
	return &UserServer{service: service, auth: auth, env: env}
}

func NewMessageServer(service message.MessageService, auth auth.AuthService, env config.Config) *MessageServer {
	return &MessageServer{service: service, auth: auth, env: env}
}

//ok, thinking of returning a HandleFunc from handlers. ie. making
//them all middleware. ?idk seems wrong

//api endpoints
func routes(r *mux.Router, env *config.Env) {
	api := r.PathPrefix("/api").Subrouter()

	auth := auth.Auth{Token: &auth.Token{}}

	var user user.User
	userServer := NewUserServer(&user, &auth, env)

	var message message.Message
	messageServer := NewMessageServer(&message, &auth, env)

	api.HandleFunc("/", auth.VerifyUser(rootHandler)).Methods("GET")
	api.HandleFunc("/login", userServer.LoginHandler).Methods("POST")
	api.HandleFunc("/chat/{token}/user/{user}", messageServer.ChatHandler).Methods("GET")
	api.HandleFunc("/chat/token/create", auth.VerifyUser(userServer.ChatTokenHandler)).Methods("GET")
	api.HandleFunc("/users/get/not", auth.VerifyUser(userServer.UsersNotHandler)).Methods("GET")
	api.HandleFunc("/messages/user/{id}", auth.VerifyUser(messageServer.RoomMessagesHandler)).Methods("GET")
}

func rootHandler(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("ok"))
}

func (u *UserServer) LoginHandler(w http.ResponseWriter, r *http.Request) {
	token, err := u.auth.LoginUser(r, u.service, u.env)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		//todo: log error
		return
	}

	//format token as json
	type res struct {
		Token string `json:"token"`
	}

	var re res
	re.Token = token
	jsonData, err := json.Marshal(re)
	if err != nil {
		fmt.Println(err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	w.Write([]byte(jsonData))
}

//this returns a token for authorized users to pass
//via url when requesting to open a chat socket
func (u *UserServer) ChatTokenHandler(w http.ResponseWriter, r *http.Request) {
	id, token, err := u.auth.CreateChatToken(w, r)
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		//todo: log error
		return
	}

	//format token as json
	type res struct {
		UserId    int    `json:"user_id"`
		ChatToken string `json:"chat_token"`
	}

	var re res
	re.UserId = id
	re.ChatToken = token

	jsonData, err := json.Marshal(re)
	if err != nil {
		fmt.Println(err)
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	w.Write([]byte(jsonData))
}

//return all users minus the current user
func (u *UserServer) UsersNotHandler(w http.ResponseWriter, r *http.Request) {
	id := u.auth.GetUserId(w, r)

	if id == 0 {
		w.WriteHeader(http.StatusBadRequest)
	}

	users, err := u.service.GetOtherUsers(id, u.env)
	if err != nil {
		//log this too
		w.WriteHeader(http.StatusBadRequest)
	}

	jsonData, err := json.Marshal(users)
	if err != nil {
		//log this too
		w.WriteHeader(http.StatusBadRequest)
	}

	w.Write(jsonData)
}

//returns past chat messages
//"id" url param is the id of the user our current
//user is requesting past messages from
func (m *MessageServer) RoomMessagesHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	idStr := vars["id"]

	connectUserId, err := strconv.Atoi(idStr)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
	}

	//get id of user requesting a chat session
	id := m.auth.GetUserId(w, r)
	messages := m.service.GetRoomMessagesByKey(id, connectUserId, m.env)

	jsonData, err := json.Marshal(messages)
	if err != nil {
		//log this too
		w.WriteHeader(http.StatusBadRequest)
	}

	w.Write(jsonData)
}

//issues a websocket to frontend client upon authorization
func (m *MessageServer) ChatHandler(w http.ResponseWriter, r *http.Request) {
	//token is a jwt token already issued by the api which
	//verifies a logged in user
	//
	//user is the user id we're connecting a chat to
	vars := mux.Vars(r)
	token := vars["token"]
	user := vars["user"]

	if token == "" {
		log.Println("chat token is empty")
		w.WriteHeader(http.StatusUnauthorized)
	}

	userId, err := m.auth.VerifyChat(token)
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusUnauthorized)
	}

	connectUserId, err := strconv.Atoi(user)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
	}

	//todo: do something with this. too many arguments
	server.ServeWebSocket(hub, userId, connectUserId, m.env, w, r)
}
