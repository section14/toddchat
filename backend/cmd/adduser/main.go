package main

import (
	"flag"
	"log"

	_ "github.com/lib/pq"
	"gitlab.com/section14/toddchat/internal/auth"
	"gitlab.com/section14/toddchat/internal/config"
)

func main() {
	//config db connection
	env := &config.Env{}
	env.Build("config.json")

	user := flag.String("user", "", "username for toddchat")
	pass := flag.String("pass", "", "password for toddchat")
	flag.Parse()

	err := auth.AddUser(*user, *pass, env)
	if err != nil {
		log.Println(err)
		return
	}

	log.Printf("user %s created\n", *user)
}
