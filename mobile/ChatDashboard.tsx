import React, {useState, useEffect} from 'react';
import ChatWindow from './ChatWindow';
import UserList from './user/UserList';

const ChatDashboard = (props: {
  showChats: boolean;
  toggleScreen: () => void;
}) => {
  interface Room {
    id: number;
    hidden: boolean;
  }

  const [rooms, addRoom] = useState<Room[]>([]);
  const [showUsers, setShowUsers] = useState(true);

  //show user list or show chatwindow
  useEffect(() => {
    if (props.showChats) {
      setShowUsers(false);
    } else {
      setShowUsers(true);

      //hide chats
      for (let i = 0; i < rooms.length; i++) {
        rooms[i].hidden = true;
      }

      //cause a re-render to update UI
      addRoom(rooms => [...rooms]);
    }
  }, [props.showChats]);

  //launch a chat window by clicking in user list
  const launchChat = (id: number) => {
    //hide user list
    props.toggleScreen();

    //make sure room doesn't exist already
    const exists = rooms.find(room => room.id === id);

    //add a new room
    if (!exists) {
      addRoom([...rooms, {id: id, hidden: false}]);
    }

    //set remaining chat windows hidden
    for (let i = 0; i < rooms.length; i++) {
      if (rooms[i].id !== id) {
        rooms[i].hidden = true;
      } else {
        rooms[i].hidden = false;
      }
    }

    //cause a re-render to update UI
    addRoom(rooms => [...rooms]);
  };

  return (
    <>
      <UserList callback={launchChat} show={showUsers} />

      {rooms.map((room, i) => {
        return <ChatWindow key={i} id={room.id} hidden={room.hidden} />;
      })}
    </>
  );
};

export default ChatDashboard;
