import React, {useRef, useEffect, useState} from 'react';
import {StyleSheet, View} from 'react-native';
import {Divider} from 'react-native-elements';
import {Message} from './message/MessageList';
import MessageList from './message/MessageList';
import MessageEntry from './message/MessageEntry';
import {getAuthData, getChatToken, api, socket} from './request/http';
import {getStore} from './request/store';

const ChatWindow = (props: {id: number; hidden: boolean}) => {
  const [userId, setUserId] = useState(0);
  const [socketOpen, setSocket] = useState(false);
  const [messages, updateMessages] = useState<Message[]>([]);
  const [msgHeight, setMsgHeight] = useState(75);

  //get a reference to global web socket created onload
  const ws = useRef<WebSocket | null>(null);

  //start up
  useEffect(() => {
    setMsgHeight(75);
    startSocket();

    return () => {
      ws.current!.close();
    };
  }, []);

  const startSocket = async () => {
    console.log('chat started');

    const token = await createChatToken().catch((e: any) => {
      console.log('error creating chat token: ', e.message);
    });

    if (token) {
      ws.current = new WebSocket(`${socket()}/chat/${token}/user/${props.id}`);
      ws.current.onopen = () => {
        console.log('socket opened');
        setSocket(true);
      };
      ws.current.onclose = () => {
        console.log('socket closed');
        setSocket(false);
        setTimeout(() => startSocket(), 5000);
      };
    }
  };

  //get previous chat session messages after userId is set
  useEffect(() => {
    const getPreviousMessages = async () => {
      const token = await getStore('token');

      const res = await getAuthData(
        `${api()}/messages/user/${props.id}`,
        token!,
      ).catch((e: any) => {
        console.log('error getting messages: ', e.message);
      });

      if (res) {
        //update messages on launch
        addPrevMessages(res);
      }
    };

    getPreviousMessages();
  }, [userId]);

  //watch socket messages
  useEffect(() => {
    if (socketOpen) {
      ws.current!.onmessage = (event: any) => {
        captureMessage(event.data);
      };
    }
  }, [socketOpen]);

  //captures message from websocket
  const captureMessage = (data: any) => {
    const json = JSON.parse(data);

    const newMessage: Message = {
      content: json.msg,
      time: '12000',
      me: sentByMe(json.user_id),
    };
    updateMessages(messages => [...messages, newMessage]);
  };

  //callback function to capture messages being sent
  const addMessage = (msg: string) => {
    ws.current!.send(msg);
  };

  const addPrevMessages = (msgArr: any) => {
    let msgs = msgArr.map(
      (msg: any) =>
        ({
          content: msg.value,
          time: msg.timestamp,
          me: sentByMe(msg.user_id), //compare user id's here
        } as Message),
    );

    //reverse list so the last message appears first
    msgs.reverse();

    updateMessages(msgs);
  };

  //format message (color, alignment) based on whether
  //or not the current user sent it
  const sentByMe = (id: number): boolean => {
    //console.log("id: ", id);
    //console.log("user id: ", userId);

    if (id === userId) {
      return true;
    }

    return false;
  };

  //create token to verify initiated chat with api
  const createChatToken = async () => {
    const token = await getStore('token');
    const res = await getChatToken(`${api()}/chat/token/create`, token!).catch(
      (e: any) => {
        console.log('bad chat token', e.message);
        return '';
      },
    );

    if (res !== null || res !== undefined) {
      setUserId(res.user_id);
      setSocket(true);
      return res.chat_token;
    }

    return '';
  };

  //modify text input height for large messages
  const changeInputHeight = (val: number) => {
    setMsgHeight(val);
  };

  return (
    <>
      <View style={props.hidden ? styles.hide : styles.container}>
        <View style={styles.list}>
          <MessageList messages={messages} />
        </View>

        <View style={[styles.msg_entry, {height: msgHeight}]}>
          <Divider orientation="horizontal" />
          <MessageEntry callback={addMessage} heightCb={changeInputHeight} />
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  hide: {
    display: 'none',
  },

  container: {
    display: 'flex',
    flex: 1,
  },

  list: {
    display: 'flex',
    flex: 1,
  },

	msg_entry: {
		minHeight: 65,
	}
});

export default ChatWindow;
