import React, {useState, useEffect} from 'react';
import {StyleSheet} from 'react-native';
import {ThemeProvider, Header} from 'react-native-elements';
import Colors from './Colors';
import LoginWindow from './user/LoginWindow';
import ChatDashboard from './ChatDashboard';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {api, postData, verifyToken} from './request/http';
import {getUserCreds, saveUserCreds, saveStore} from './request/store';
import {startNetworkLogging} from 'react-native-network-logger';
//import NetworkLogger from 'react-native-network-logger';

const theme = {
  Text: {
    style: {
      fontSize: 20,
    },
  },

  Button: {
    titleStyle: {
      fontSize: 20,
    },
  },
};

startNetworkLogging();

const App = () => {
  const [loggedIn, userLogin] = useState(false);
  const [icon, setIcon] = useState('menu');
  const [showChats, setShowChats] = useState(false);

  useEffect(() => {
    //get locally stored login token
    const getToken = async (): Promise<boolean> => {
      try {
        const token = await AsyncStorage.getItem('token');

        //attemp to validate token
        if (token === null || token === '') {
          return false;
        }

        const check = await checkToken(token);
        if (check) {
          return true;
        }
      } catch (e) {
        console.log(e);
      }

      return false;
    };

    //if the token is valid, set ok and move on
    const tryLogin = async () => {
      const validToken = await getToken();

      if (validToken) {
        userLogin(true);
        return;
      }

      //attempt to login user with saved credentials
      const creds = await getUserCreds();

      //credentials were returned, so attempt a login
      //if this fails, the login screen will be shown
      if (creds) {
        console.log('username', creds.username);
        console.log('password', creds.password);
        loginFromSaved(creds!.username, creds!.password);
      }
    };

		tryLogin();

  }, []);

  const loginFromSaved = async (user: string, pass: string) => {
    interface Login {
      username: string;
      password: string;
    }

    const loginData: Login = {
      username: user,
      password: pass,
    };

    let res = await postData(`${api()}/login`, loginData).catch((e: any) => {
      console.log('login error: ', e.message);
      return;
    });

    //check for undefined response
    if (res === undefined) {
      userLogin(false);
      return;
    }

    //if res is populated, set logged in status
    saveStore('token', res.token);
    userLogin(true);
  };

  //function to check jwt token and verify login
  const checkToken = async (token: string): Promise<boolean> => {
    const res = await verifyToken(`${api()}/`, token!).catch((e: any) => {
      //idk
			console.log('api: ', api());
      console.log('bad chat token: ', e.message);
    });

    if (res) {
      return true;
    }

    return false;
  };

  //
  const loginCallback = (ok: boolean) => {
    if (ok) {
      userLogin(true);
      return;
    }

    userLogin(false);
  };

  //
  const toggleScreen = () => {
    if (showChats) {
      setIcon('menu');
    } else {
      setIcon('west');
    }

    setShowChats(!showChats);
  };

  return (
    <ThemeProvider theme={theme}>
      <Header
        leftComponent={{
          icon: `${icon}`,
          color: '#fff',
          iconStyle: {color: '#fff'},
          onPress: () => toggleScreen(),
        }}
        centerComponent={{
          text: 'Todd Chat',
          style: {color: '#fff', fontSize: 20},
        }}
        containerStyle={{paddingTop: 40, paddingBottom: 20}}
      />

      {loggedIn ? null : <LoginWindow callback={loginCallback} />}
      {loggedIn ? (
        <ChatDashboard showChats={showChats} toggleScreen={toggleScreen} />
      ) : null}

      {/*
      <View style={{height: '50%'}}>
        <NetworkLogger />
      </View>
			*/}
    </ThemeProvider>
  );
};

const styles = StyleSheet.create({
  background: {
    flex: 1,
    backgroundColor: Colors.light,
  },
});

export default App;
