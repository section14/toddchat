import {getAuthData, api} from '../request/http';
import {getStore} from '../request/store';
import React, {useEffect, useState} from 'react';
import {View, StyleSheet} from 'react-native';
import {ListItem} from 'react-native-elements';

interface User {
  id: number;
  name: string;
  current: boolean;
}

const UserList = (props: {
  show: boolean;
  callback: (msg: number) => void;
}) => {
  //startup call
  useEffect(() => {
    getUsers();
  }, []);

  const [users, setUser] = useState<User[]>([]);

  //get all users that aren't the current user
  const getUsers = async () => {
    //const token = localStorage.getItem("token");
    const token = await getStore('token');

    const res = await getAuthData(`${api()}/users/get/not`, token!).catch(
      (e: any) => {
        console.log("couldn't get users: ", e.message);
      },
    );

    if (res) {
      addUsers(res);
    }
  };

  //add returned users to list for selection
  const addUsers = (res: any) => {
    let chatUsers = res.map(
      (usr: any) =>
        ({
          id: usr.id,
          name: usr.name,
        } as User),
    );

    setUser(chatUsers);
  };

  //pass selected user up with callback function
  const selectUser = (key: number) => {
    props.callback(key);
  };

  return (
    <>
      <View style={props.show ? styles.show : styles.hide}>
        {users.map((user, i) => (
          <ListItem key={i} onPress={() => selectUser(user.id)} bottomDivider>
            <ListItem.Content>
              <ListItem.Title>{user.name}</ListItem.Title>
            </ListItem.Content>
          </ListItem>
        ))}
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  hide: {
    display: 'none',
  },
  show: {
    display: 'flex',
  },
});

export default UserList;
