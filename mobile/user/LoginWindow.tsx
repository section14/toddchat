import React, {useState, useEffect} from 'react';
import {api, postData} from '../request/http';
import {View} from 'react-native';
import {Text, Input, Button} from 'react-native-elements';
import {saveStore} from '../request/store';

const LoginWindow = (props: {callback: (ok: boolean) => void}) => {
  interface Login {
    username: string;
    password: string;
  }

  const [loginData, setLogin] = useState<Login>({username: '', password: ''});
  const [loginStatus, setStatus] = useState('');

  //start up
  useEffect(() => {
    console.log('app started');
  }, []);

  //handle form change
  const handleInputChange = (name: string, value: any) => {
    setLogin({...loginData, [name]: value});
  };

  //attempt to log user in
  const loginUser = async () => {
    let res = await postData(`${api()}/login`, loginData).catch((e: any) => {
      console.log('login error: ', e.message);
      return;
    });

    //check for undefined response
    if (res === undefined) {
      setStatus('login failed');
      props.callback(false);
      return;
    }

    //set status - mainly for testing
    setStatus('logged in');

    //set user token
		saveStore('token', res.token)

    //signal to callback we logged in
    props.callback(true);
  };

  return (
    <View>
      <Input
        placeholder="username"
        onChangeText={value => handleInputChange('username', value)}
      />

      <Input
        placeholder="password"
        secureTextEntry={true}
        onChangeText={value => handleInputChange('password', value)}
      />

      <Button
        title="Login"
        onPress={() => {
          loginUser();
        }}
      />

      <Text>{loginStatus}</Text>
    </View>
  );
};

export default LoginWindow;
