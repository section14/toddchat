////////////////////////////////////////////////////////////////////
// api references from env variables ///////////////////////////////
////////////////////////////////////////////////////////////////////

export const api = () => {
  //return process.env.REACT_APP_CHAT_API;
	return "https://chat.sectionfourteen.com/api"
};

export const socket = () => {
  //return process.env.REACT_APP_CHAT_WS;
	return "wss://chat.sectionfourteen.com/api"
};

////////////////////////////////////////////////////////////////////
// date functions //////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////

export const formatDate = (d: string) => {
  //takes a unix time stamp and convers it to
  //a human readable string. ex. 10-5-18
  let date = new Date(d);
  let day = date.getDate();
  let month = date.getMonth() + 1; //why?
  let year = date.getFullYear().toString().substr(2, 2);
  let dateStr = month + "-" + day + "-" + year;

  //todo: add minutes

  return dateStr;
};

////////////////////////////////////////////////////////////////////
// ajax requests ///////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////

export const postData = async (
  url: RequestInfo = "",
  req: Object = {}
): Promise<any> => {
  const res = await fetch(url, {
    method: "POST",
    mode: "cors",
    cache: "no-cache",
    credentials: "same-origin",
    headers: {
      "Content-Type": "application/json",
      "X-Requested-With": "XMLHttpRequest",
    },
    redirect: "follow",
    referrerPolicy: "no-referrer",
    body: JSON.stringify(req),
  });

  let data = {};

  //todo: handle responses that aren't json
  if (!res.ok) {
    throw new Error(`Post error: ${res.status}`);
  } else {
    data = await res.json();
  }

  return data;
};

export const getData = async (url: RequestInfo = "") => {
  const res = await fetch(url, {
    method: "GET",
    mode: "cors",
    cache: "no-cache",
    credentials: "same-origin",
    headers: {
      "Content-Type": "application/json",
      "X-Requested-With": "XMLHttpRequest",
    },
    redirect: "follow",
    referrerPolicy: "no-referrer",
    //body: JSON.stringify(req),
  });

  let data = {};

  //todo: handle responses that aren't json
  if (!res.ok) {
    throw new Error(`Get error: ${res.status}`);
  } else {
    data = await res.json();
  }

  return data;
};

//get request which sends a request token for authorization
export const getAuthData = async (
  url: RequestInfo = "",
  token: string = ""
) => {
  const res = await fetch(url, {
    method: "GET",
    mode: "cors",
    cache: "no-cache",
    credentials: "same-origin",
    headers: {
      "Content-Type": "application/json",
      "X-Requested-With": "XMLHttpRequest",
      "User-Token": token,
    },
    redirect: "follow",
    referrerPolicy: "no-referrer",
    //body: JSON.stringify(req),
  });

  let data = {};

  //todo: handle responses that aren't json
  if (!res.ok) {
    throw new Error(`Get error: ${res.status}`);
  } else {
    data = await res.json();
  }

  return data;
};

export const verifyToken = async (
  url: RequestInfo = "",
  token: string = ""
) => {
  const res = await fetch(url, {
    method: "GET",
    mode: "cors",
    cache: "no-cache",
    credentials: "same-origin",
    headers: {
      "Content-Type": "text/plain",
      "X-Requested-With": "XMLHttpRequest",
      "User-Token": token,
    },
    redirect: "follow",
    referrerPolicy: "no-referrer",
  });

  //todo: handle responses that aren't json
  if (!res.ok) {
    throw new Error(`Token error: ${res.status}`);
  }

  return res;
};

export const getChatToken = async (
  url: RequestInfo = "",
  token: string = ""
): Promise<any> => {
  const res = await fetch(url, {
    method: "GET",
    mode: "cors",
    cache: "no-cache",
    credentials: "same-origin",
    headers: {
      "Content-Type": "application/json",
      "X-Requested-With": "XMLHttpRequest",
      "User-Token": token,
    },
    redirect: "follow",
    referrerPolicy: "no-referrer",
    //body: JSON.stringify(req),
  });

  let data = {};

  //todo: handle responses that aren't json
  if (!res.ok) {
    throw new Error(`Get error: ${res.status}`);
  } else {
    data = await res.json();
  }

  return data;
};
