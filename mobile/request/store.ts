import AsyncStorage from '@react-native-async-storage/async-storage';
import * as Keychain from 'react-native-keychain';

//save user credentials to device keychain
export const saveUserCreds = async (user: string, pass: string) => {
	await Keychain.setGenericPassword(user,pass);
}

export const getUserCreds = async (): Promise<false | Keychain.UserCredentials> => {
	const creds = await Keychain.getGenericPassword();

	if (creds) {
		return creds;	
	}

	return false;
}

//save data to local device storage
export const saveStore = async (
  key: string,
  value: string,
): Promise<boolean> => {
  try {
    await AsyncStorage.setItem(key, value);
  } catch (e) {
    console.log(e);
    return false;
  }

  return true;
};

//get data from local device storage
export const getStore = async (key: string): Promise<string> => {
  let token = '';

  try {
    const storedToken = await AsyncStorage.getItem(key);

    //validate token
    if (storedToken === null || storedToken === '') {
      return '';
    }

    token = storedToken!;
  } catch (e) {
    console.log(e);
  }

  return token;
};
