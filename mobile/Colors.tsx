export default {
	primary: "#1A237E",
	secondary: "#EC407A",
	secondary_light: "#d0ded6",
	dark: "#212121",
	light: "#E8EAF6",
	grey: "#999999",

	me_bubble: "#d0ded6",
	me_bubble_font: "#244e37",
}
