import React, {useState, useEffect} from 'react';
import {Input, Button, Icon} from 'react-native-elements';
import {StyleSheet, Keyboard, View} from 'react-native';
import Colors from '../Colors';

const MessageEntry = (props: {
  callback: (msg: string) => void;
  heightCb: (val: number) => void;
}) => {
  const [msg, setMessage] = useState('');
  const [msgHeight, setMsgHeight] = useState(75);

	useEffect(()=>{
		console.log("why are you: ", msgHeight)
	}, [msgHeight])

  //handle text change
  const handleInputChange = (value: string) => {
    setMessage(value);
  };

  //send message up via callback
  const sendMessage = () => {
    props.callback(msg);
    Keyboard.dismiss();
    setMessage('');
		setMsgHeight(45.09);
		props.heightCb(0);
  };

  const setInputTextHeight = (val: number) => {
    setMsgHeight(val);
		props.heightCb(val);
  };

  return (
    <View style={styles.row}>
      <View style={styles.textInput}>
        <Input
          placeholder="enter a message"
          inputContainerStyle={{borderBottomWidth: 0}}
          multiline={true}
          value={msg}
          onChangeText={value => handleInputChange(value)}
          onContentSizeChange={event => {
            setInputTextHeight(event.nativeEvent.contentSize.height);
          }}
          inputStyle={[{height: msgHeight}]}
        />
      </View>

      <View style={styles.sendButton}>
        <Button
          icon={<Icon name="send" size={26} color={Colors.grey} />}
          type="clear"
          onPress={() => {
            sendMessage();
          }}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  row: {
    display: 'flex',
    flexDirection: 'row',
    flexGrow: 1,
    paddingTop: 10,
  },

  textInput: {
    display: 'flex',
    flexDirection: 'column',
    flexGrow: 12,
  },

  sendButton: {
    display: 'flex',
    flexDirection: 'column',
    flexGrow: 1,
  },
});

export default MessageEntry;
