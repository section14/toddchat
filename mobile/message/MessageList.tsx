import React, {useRef} from 'react';
import {StyleSheet, ScrollView, View} from 'react-native';
import {Text} from 'react-native-elements';
import Colors from '../Colors';

export interface Message {
  content: string;
  time: string;
  me: boolean;
}

const MessageLine = (props: {msg: string; me: boolean}) => {
  return (
    <View style={[styles.row, props.me ? styles.meRow : styles.themRow]}>
      <View>
        <Text
          style={[
            props.me ? styles.meBubble : styles.themBubble,
            styles.bubble,
          ]}>
          {props.msg}
        </Text>
      </View>
    </View>
  );
};

const MessageList = (props: {messages: Message[]}) => {
  const scrollRef = useRef<ScrollView>(null);

  return (
    <View>
      <ScrollView
        ref={scrollRef}
        onContentSizeChange={() => scrollRef.current!.scrollToEnd()}>
        {props.messages.map(msg => {
          return (
            <MessageLine
              key={Math.random().toString(36).substr(2, 9)}
              msg={msg.content}
              me={msg.me}
            />
          );
        })}
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  row: {
    display: 'flex',
    flexDirection: 'row',
    flexGrow: 1,
    paddingTop: 10,
  },

  meRow: {
    justifyContent: 'flex-end',
    paddingRight: 15,
  },

  themRow: {
    justifyContent: 'flex-start',
    paddingLeft: 15,
  },

  bubble: {
    display: 'flex',
    flexDirection: 'row',
		flexWrap: 'wrap',
    fontSize: 16,
    borderRadius: 20,
    flexGrow: 0,
    flexShrink: 0,
		flexBasis: '40%',
    padding: 10,
		maxWidth: 250,
  },

  meBubble: {
    backgroundColor: Colors.me_bubble,
    color: Colors.me_bubble_font,
  },

  themBubble: {
    backgroundColor: Colors.light,
  },
});

export default MessageList;
